import bsqlite3 from 'better-sqlite3';
import fs from 'fs';

const databasePath = `${process.env.DATABASE_DIR ?? './databases'}`

if (!fs.existsSync(databasePath)) {
    fs.mkdirSync(databasePath, {recursive: true})
}

const database = bsqlite3(`${databasePath}/database.db`)

export default database
export function initDatabase() {
    database.prepare('CREATE TABLE IF NOT EXISTS user (id TEXT, login TEXT)').run();
}