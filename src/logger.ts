import createDebug, { Debugger } from 'debug';
import fs, { WriteStream } from 'fs';
import util from 'util';

let today = new Date().toISOString().split('T')[0];
let logPrefix = `./logs/${today}`;

if (!fs.existsSync(logPrefix)) {
    fs.mkdirSync(logPrefix, {recursive: true})
}

let serverStream = fs.createWriteStream(`${logPrefix}/server.log`, {flags: 'a'});

let streams = new Map<string, WriteStream>();
let debugs = new Map<string, Debugger>();

process.env.DEBUG?.split(' ')?.forEach(d => {
    let key = d.toLowerCase();
    streams.set(key, fs.createWriteStream(`${logPrefix}/${key}.log`, {flags: 'a'}));
    let debug = createDebug(d);
    debug.log = function(...args: any[]) {
        serverStream.write(`[${new Date().toISOString()}]\t[${this.namespace}]\t- ${util.formatWithOptions({}, ...this.arguments)}\n`);
        streams.get(key)?.write(`[${new Date().toISOString()}]\t[${this.namespace}]\t- ${util.formatWithOptions({}, ...this.arguments)}\n`);
        createDebug.log(...args);
    }
    debugs.set(d.toLowerCase(), debug)
});


export default function logger(namespace: string): Debugger {
    return debugs.get(namespace.toLowerCase()) ?? createDebug('NOT_FOUND')
}