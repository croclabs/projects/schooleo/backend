import { NextFunction, Request, Response } from "express";
import logger from "./logger";
import createError from 'http-errors';
import { ResError } from "./errors";

export function requestLog(req: Request, res: Response, next: NextFunction) {
    logger('request')('%s %s', req.method, req.path)
    next()
}

export function catch404(req: Request, res: Response, next: NextFunction) {
    next(createError(404));
}

export function errorHandler(err: ResError, req: Request, res: Response) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status ?? 500);
    res.render('error');
}